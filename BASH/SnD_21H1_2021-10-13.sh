#!/bin/env bash
dependencies="git 7z/p7zip-full find/findutils"
AME_LIST="./AME_List_$(date +'%Y_%m_%d-%H_%M_%S').txt"

continuation_prompt() {
   read -r -p "To continue press [ENTER], or Ctrl-C to exit"
}

title_bar() {
   clear
   echo "╔═════════════════════════════════════════════════════╗"
   echo "║ AMEliorate Windows 10 21H1               2021.10.13 ║"
   echo "╚═════════════════════════════════════════════════════╝"
   echo "                                                       "
}

clear
echo "                 ╔═══════════════╗                   "
echo "                 ║ !!!WARNING!!! ║                   "
echo "╔════════════════╩═══════════════╩══════════════════╗"
echo "║ This script comes without any warranty.           ║"
echo "║ If your computer no longer boots, explodes, or    ║"
echo "║ divides by zero, you are the only one responsible ║"
echo "╟╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╢"
echo "║ This script only works on Ubuntu based distros.   ║"
echo "║ An Ubuntu Live ISO is recommended.                ║"
echo "╚═══════════════════════════════════════════════════╝"
echo "                                                     "
continuation_prompt

# prompts to install git and 7zip if not already installed
title_bar
if command -v apt >/dev/null; then
   echo "This script requires the installation of a few"
   echo "dependencies. Please enter your password below."
   echo
   sudo apt update
   for dep in ${dependencies}; do
      exe="${dep%%/*}"
      pkg="${dep#*/}"
      command -v "${exe}" >/dev/null || {
         echo "Command ${exe} not found, installing ..."
         sudo apt-get -y install "${pkg}"
      }
   done
else
   echo 'This script must be run on an Ubuntu based distribution.'
   echo 'You can continue anyways, the following dependencies are required:'
   echo
   echo "${dependencies}"
   echo
   continuation_prompt
fi

[ -e "${AME_LIST}" ] && {
   echo "Error! File '${AME_LIST}' exists."
   exit 1
}

# start AME process
title_bar
echo "Searching for files (This might take a long time) ..."

Whitelist=('.*AppLocker.*'
           '.*autologger.*'
           '.*clipup.*'
           '.*DeliveryOptimization.*'
           '.*/DeviceCensus.exe$'
           '.*diagtrack.*'
           '.*dmclient.*'
           '.*dosvc.*'
           '.*EnhancedStorage.*'
           '.*HomeGroup.*'
           '.*hotspot.*'
           '.*invagent.*'
           '.*Microsoft ?Edge.*'
           '.*msra.*'
           '.*sihclient.*'
           '.*slui.*'
           '.*startupscan.*'
           '.*storsvc.*'
           '.*usoapi.*'
           '.*usoclient.*'
           '.*usocore.*'
           '.*usocoreworker.*'
           '.*usosvc.*'
           '.*WaaS.*'
           '.*windowsmaps.*'
           '.*windowsupdate.*'
           '.*wsqmcons.*'
           '.*wua.*'
           '.*wus.*'
           '.*wups.*'
           '.*Xbox.*'
           '.*Internet ?Explorer.*'
           '.*CloudExperienceHost.*'
           '.*ContentDeliveryManager.*'
           '.*SecHealth.*'
           '.*Security ?Health.*'
           '.*Program ?Files.*/Microsoft'
           '.*Program ?\(Data\|Files\).*/Windows Defender.*'
           '.*Program ?\(Data\|Files\).*/Windows Mail'
           '.*Program ?\(Data\|Files\).*/Windows Media Player'
           '.*Program ?\(Data\|Files\).*/Windows Photo Viewer'
           '.*Program ?\(Data\|Files\).*/Windows Security'
           '.*/Microsoft\.Windows\.Cortana.*'
           '.*/system/Apps'
           '.*/system/WindowsUpdate'
           '.*WindowsApps.*'
           '.*UpdateTargeting.*'
           '.*OneDrive.*'
           '.*telemetry.*'
           '.*smartscreen.*')

Blacklist=('.*MSRAW.*'                    '.*WinSxS.*'
           '.*FileMaps.*'                 '.*msrating.*'
           '.*AME_Backup.*'               '.*Microsoft\.NET.*'
           '.*Microsoft\.MSPaint.*'       '.*Windows\.Search.*'
           '.*ImmersiveControlPanel.*'    '.*DesktopAppInstaller.*'
           '.*Windows\.XGpuEjectDialog.*' '.*ShellExperienceHost.*')

# This might look odd, but find operates much faster that way.
IFS=$'\n' Whitelist="$(echo -n "${Whitelist[*]}" | sed -z 's/\n/\\|/g')"
IFS=$'\n' Blacklist="$(echo -n "${Blacklist[*]}" | sed -z 's/\n/\\|/g')"

find . -not -iregex "${Blacklist}" -iregex "${Whitelist}" -prune >"${AME_LIST}"

[ -s "${AME_LIST}" ] || {
   echo "ERROR! No files found."
   exit 1
}

echo "You may now (optionally) audit and edit ${AME_LIST}"
continuation_prompt

cat >AME_Remove.sh <<'EOF'
#!/bin/env sh
[ -z "${1}" ] && {
   echo 'usage: AME_Remove.sh <Path to AME_List>'
   exit 1
}

while read -r file; do
   dir="$(dirname "${file}")"
   mkdir -p "./AME_Backup/${dir}"
   echo "-> '${file}'"
   mv -n "${file}" --target-directory="./AME_Backup/${dir}"
   rmdir -p "./${dir}" 2>/dev/null
done <"${1}"

sync
EOF

cat >AME_Restore.sh <<'EOF'
#!/bin/env sh
[ -z "${1}" ] && {
   echo 'usage: AME_Restore.sh <Path to AME_List>'
   exit 1
}

while read -r file; do
   dir="$(dirname "${file}")"
   mkdir -p "./${dir}"
   echo "-> '${file}'"
   mv -n "./AME_Backup/${file}" --target-directory="${dir}"
   rmdir -p "./AME_Backup/${dir}" 2>/dev/null
done <"${1}"

sync
EOF

echo "Backing up files"
sh ./AME_Remove.sh "${AME_LIST}"

echo
echo
du -hs AME_Backup
echo "You may now reboot into Windows"
